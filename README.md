# trivia-questions-frontend

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Project Demo

<p>The system shows the category and difficulty level of each question</p>

<!-- right-choice.png -->

![Alt text](public/Markdown/images/right-choice.png "The UI is dynamic if a question is easy then the bg is green, red if hard and amber if medium")

### Example of a hard question 
![Alt text](public/Markdown/images/hard-question.png "The UI is dynamic if a question is easy then the bg is green, red if hard and amber if medium")


### Example if user picks right choice
![Alt text](public/Markdown/images/right-choice.png "The UI is dynamic if user picks right choice then the answer is placed onthe component green if correct and red otherwise")


### Example if user picks a wrong choice
![Alt text](public/Markdown/images/wrong-choice.png "The UI is dynamic if user picks right choice then the answer is placed onthe component green if correct and red otherwise")




