// Import Bootstrap and BootstrapVue CSS files (order is important)
import { createApp } from 'vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'


createApp(App).mount('#app')
